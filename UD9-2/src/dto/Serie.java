package dto;

public class Serie implements Entregable {
	//Atruibutos por defecto
	final int DnumTemp=3;
	final boolean Dentregado=false;
	final String DString = "";
	
	//Atributos
	private String titulo;
	private int numTemp;
	private boolean entregado;
	private String genero;
	private String creador;
	
	//constructor vacio
	public Serie() {
		this.titulo = DString;
		this.numTemp = DnumTemp;
		this.entregado = Dentregado;
		this.genero = DString;
		this.creador = DString;
	}
	
	//constructor solo titulo creador
	public Serie(String titulo, String creador) {
		
		this.titulo = titulo;
		this.numTemp = DnumTemp;
		this.entregado = Dentregado;
		this.genero = DString;
		this.creador = creador;
	}

	
	//Constructor completo
	
	public Serie(String titulo, int numTemp, String genero, String creador) {
		this.titulo = titulo;
		this.numTemp = numTemp;
		this.entregado = Dentregado;
		this.genero = genero;
		this.creador = creador;
	}
	
	//Getters i setters
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getNumTemp() {
		return numTemp;
	}

	public void setNumTemp(int numTemp) {
		this.numTemp = numTemp;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getCreador() {
		return creador;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}

	//To String 
	@Override
	public String toString() {
		return "Serie [titulo=" + titulo + ", numTemp=" + numTemp + ", entregado=" + entregado + ", genero=" + genero
				+ ", creador=" + creador + "]";
	}
	
	//Metedos para canviar el booleano entregado
	
	public void entregar() {
		entregado=true;
	}
	
	public void devolver() {
		entregado=false;
	}
	
	//getter entregado
	public boolean isEntregado() {
		return  entregado;
	}
	
	//Comprarar numero temporadas de dos series
	public Object compareTo(Object a) {
		Serie se = (Serie)a;
		if(this.numTemp>se.getNumTemp()) {
			se=this;
		}
		return se;
	}
	
}
