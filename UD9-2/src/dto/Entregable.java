package dto;

public interface Entregable {
//Interface para Serie i videojuego
	public void entregar();
	public void devolver();
	public boolean isEntregado();
	public Object compareTo(Object a);
}
