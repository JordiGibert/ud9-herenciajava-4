package dto;


public class VideoJuego implements Entregable {

	
		/*Crearemos una clase Videojuego con las siguientes caracter�sticas:
		� Sus atributos son titulo, horas estimadas, entregado, genero y compa�ia.
		� Por defecto, las horas estimadas ser�n de 10 horas y entregado false. El resto
		de atributos ser�n valores por defecto seg�n el tipo del atributo.*/
		
		
		//Atributos por defecto
		final int DHoras=10;
		final boolean Dentregado=false;
		final String DString = "";
		
		//Atributos
		private String titulo;
		private int horas_estimadas;
		private boolean entregado;
		private String genero;
		private String compa�ia;
		
		//Constructor vacio
		public VideoJuego() {
			this.titulo = DString;
			this.horas_estimadas = DHoras;
			this.entregado = Dentregado;
			this.genero = DString;
			this.compa�ia = DString;
		
		}
		
		//constructor titulo y compa�ia
		public VideoJuego(String titulo, String compa�ia) {
			
			this.titulo = titulo;
			this.horas_estimadas = DHoras;
			this.entregado = Dentregado;
			this.genero = DString;
			this.compa�ia = compa�ia;
		}

		//constructor completo
		public VideoJuego(String titulo, int horas_estimadas, String genero, String compa�ia) {
			this.titulo = titulo;
			this.horas_estimadas = horas_estimadas;
			this.entregado = Dentregado;
			this.genero = genero;
			this.compa�ia = compa�ia;
		}
		
		//To String i getters i setters
		public String toString() {
			return "VideoJuego [titulo=" + titulo + ", horas_estimadas=" + horas_estimadas + ", entregado=" + entregado + ", genero=" + genero
					+ ", compa�ia=" + compa�ia + "]";
		}
		
		
		/**
		 * @return the titulo
		 */
		public String getTitulo() {
			return titulo;
		}
		/**
		 * @param titulo the titulo to set
		 */
		public void setTitulo(String titulo) {
			this.titulo = titulo;
		}
		/**
		 * @return the horas_estimadas
		 */
		public int getHoras_estimadas() {
			return horas_estimadas;
		}
		/**
		 * @param horas_estimadas the horas_estimadas to set
		 */
		public void setHoras_estimadas(int horas_estimadas) {
			this.horas_estimadas = horas_estimadas;
		}
		/**
		 * @return the entregado
		 */
		public boolean isEntregado() {
			return entregado;
		}
		/**
		 * @param entregado the entregado to set
		 */
		public void setEntregado(boolean entregado) {
			this.entregado = entregado;
		}
		/**
		 * @return the genero
		 */
		public String getGenero() {
			return genero;
		}
		/**
		 * @param genero the genero to set
		 */
		public void setGenero(String genero) {
			this.genero = genero;
		}
		/**
		 * @return the compa�ia
		 */
		public String getCompa�ia() {
			return compa�ia;
		}
		/**
		 * @param compa�ia the compa�ia to set
		 */
		public void setCompa�ia(String compa�ia) {
			this.compa�ia = compa�ia;
		}
		
		//CAnviar booleano entregar
		public void entregar() {
			entregado=true;
		}
		
		public void devolver() {
			entregado=false;
		}
		
	
		//Comprar numero de horas de dos videojuegos
		public Object compareTo(Object a) {
			VideoJuego se = (VideoJuego) a;
			if(this.horas_estimadas>=se.getHoras_estimadas()) {
				se=this;
			}
			return se;
		}
		

}
