import java.util.ArrayList;

import dto.*;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Serie serie1=new Serie();
		Serie serie2=new Serie("Pocoyo","Pere Marti");
		Serie serie3=new Serie("Els barrofets", 50, "infantil", "Juan Garcia");
		Serie serie4=new Serie("Telletubies", 20, "infantil", "Pep Castillo");
		Serie serie5=new Serie("Gatigos","Joana Lopez");
		
		/*System.out.println(serie1.toString());
		System.out.println(serie2.toString());
		System.out.println(serie3.toString());
 */
		ArrayList<Serie> series=new ArrayList<Serie>();
		series.add(serie1);
		series.add(serie2);
		series.add(serie3);
		series.add(serie4);
		series.add(serie5);
		
		//A�adir videojuegos
		
		VideoJuego joc1=new VideoJuego();
		VideoJuego joc2=new VideoJuego("Call of duty","Activision");
		VideoJuego joc3=new VideoJuego("Mario",40,"plataformas","Nintendo");
		VideoJuego joc4=new VideoJuego("Zelda",100,"aventuras","Nintendo");
		VideoJuego joc5=new VideoJuego("Minecraft",1000,"sandbox","Mojang");
		
		ArrayList<VideoJuego> juegos=new ArrayList<VideoJuego>();
		juegos.add(joc1);
		juegos.add(joc2);
		juegos.add(joc3);
		juegos.add(joc4);
		juegos.add(joc5);
		
		//entregar jocs i series
		joc4.entregar();
		joc3.entregar();
		serie2.entregar();
		serie5.entregar();
		serie3.entregar();
		
		//tornar series i jocs
		int contadorS =0;
		for(int i = 0; i< series.size(); i++) {
			if(series.get(i).isEntregado()==true) {
				contadorS++;
				series.get(i).devolver();
			}
		}
	
		int contadorV=0;
		for(int i = 0; i< juegos.size(); i++) {
			if(juegos.get(i).isEntregado()==true) {
				contadorV++;
				juegos.get(i).devolver();
			}
		}
		
		System.out.println("Se han devuelto "+contadorS+" series y "+contadorV+" juegos.");
		//serie y juego con mas horas 
		int numS=0;
		for(int i = 1;i<series.size();i++) {
			Serie serie = (Serie) series.get(numS).compareTo(series.get(i));
			if(serie.equals(series.get(i))) {
				numS=i;
			}
		}
			
		System.out.print("La serie con mas horas es: ");
		System.out.println(series.get(numS).toString());
		
		int numV=0;
		for(int i = 1;i<juegos.size();i++) {
			VideoJuego juego = (VideoJuego) juegos.get(numV).compareTo(juegos.get(i));
			if(juego.equals(juegos.get(i))) {
				numV=i;
			}
		}
			
		System.out.print("El juego con mas horas es: ");
		System.out.println(juegos.get(numV).toString());
	}
	
	

}
